places = {
    "id-pole_6": {
        "location": {
            "lat": 50.3245637,
            "lng": 31.5122443
        },
        "address": "\u0411\u0435\u0440\u0435\u0437\u0430\u043d\u044c, \u0443\u043b. \u041c\u0430\u044f\u043a\u043e\u0432\u0441\u044c\u043a\u043e\u0433\u043e, 10",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_8": {
        "location": {
            "lat": 51.1383785,
            "lng": 31.7776322
        },
        "address": "\u0411\u043e\u0431\u0440\u0438\u043a, \u0432\u0443\u043b. \u041d\u0430\u0431\u0435\u0440\u0435\u0436\u043d\u0430, 21",
        "type": 1,
        "services": [{
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_9": {
        "location": {
            "lat": 50.3658265,
            "lng": 30.9210091
        },
        "address": "\u0411\u043e\u0440\u0438\u0441\u043f\u0456\u043b\u044c, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u043a\u0438\u0439 \u0448\u043b\u044f\u0445, 2\u0431",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_10": {
        "location": {
            "lat": 50.3657993,
            "lng": 30.9855571
        },
        "address": "\u0411\u043e\u0440\u0438\u0441\u043f\u0456\u043b\u044c, \u0432\u0443\u043b. \u041f\u0440\u0438\u0432\u043e\u043a\u0437\u0430\u043b\u044c\u043d\u0430, 88",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_11": {
        "location": {
            "lat": 50.4913414,
            "lng": 30.741289
        },
        "address": "\u0411\u0440\u043e\u0432\u0430\u0440\u0438, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430, 2\u0430",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_12": {
        "location": {
            "lat": 50.5143181,
            "lng": 30.7827679
        },
        "address": "\u0411\u0440\u043e\u0432\u0430\u0440\u0438, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430, 227",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_13": {
        "location": {
            "lat": 50.5271161,
            "lng": 30.7936507
        },
        "address": "\u0411\u0440\u043e\u0432\u0430\u0440\u0438, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430, 320",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_14": {
        "location": {
            "lat": 50.5049746,
            "lng": 30.7945041
        },
        "address": "\u0411\u0440\u043e\u0432\u0430\u0440\u0438, \u0432\u0443\u043b. \u042f\u0440\u043e\u0441\u043b\u0430\u0432\u0430 \u041c\u0443\u0434\u0440\u043e\u0433\u043e, 57",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_15": {
        "location": {
            "lat": 50.5174787,
            "lng": 30.8087298
        },
        "address": "\u0411\u0440\u043e\u0432\u0430\u0440\u0438, \u0421\u0438\u043c\u043e\u043d\u0430 \u041f\u0435\u0442\u043b\u044e\u0440\u0438, 11\u0431",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_16": {
        "location": {
            "lat": 50.5107235,
            "lng": 30.8455619
        },
        "address": "\u0411\u0440\u043e\u0432\u0430\u0440\u0438, \u0432\u0443\u043b. \u0411\u0440\u043e\u0432\u0430\u0440\u0441\u044c\u043a\u043e\u0457 \u0441\u043e\u0442\u043d\u0456, 15\u0430",
        "type": 1,
        "services": [{
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_17": {
        "location": {
            "lat": 50.2328435,
            "lng": 30.3216956
        },
        "address": "\u0412\u0430\u0441\u0438\u043b\u044c\u043a\u0456\u0432, \u0432\u0443\u043b. \u0414\u0435\u043a\u0430\u0431\u0440\u0438\u0441\u0442\u0456\u0432, 406",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_18": {
        "location": {
            "lat": 50.3917253,
            "lng": 30.3630996
        },
        "address": "\u0412\u0438\u0448\u043d\u0435\u0432\u0435, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430, 2\u0433",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_19": {
        "location": {
            "lat": 50.3882889,
            "lng": 30.3921962
        },
        "address": "\u0412\u0438\u0448\u043d\u0435\u0432\u0435, \u0432\u0443\u043b. \u0427\u043e\u0440\u043d\u043e\u0432\u043e\u043b\u0430, 54",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_14",
            "name": "\u0411\u043e\u0440\u0449BURGER",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_20": {
        "location": {
            "lat": 50.2546003,
            "lng": 30.3047588
        },
        "address": "\u0413\u043b\u0435\u0432\u0430\u0445\u0430, \u0432\u0443\u043b. \u0412\u043e\u043a\u0437\u0430\u043b\u044c\u043d\u0430, 2\u0430",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_21": {
        "location": {
            "lat": 49.6553303,
            "lng": 30.3478016
        },
        "address": "\u0414\u043e\u0432\u0433\u0430\u043b\u0456\u0432\u0441\u044c\u043a\u0435, \u0432\u0443\u043b. \u0414\u0432\u043e\u0440\u044f\u043d\u0441\u044c\u043a\u0430, 1\u0430",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_22": {
        "location": {
            "lat": 50.586397,
            "lng": 30.83959
        },
        "address": "\u0412\u0435\u043b\u0438\u043a\u0430 \u0414\u0438\u043c\u0435\u0440\u043a\u0430, \u0432\u0443\u043b. \u041b\u0435\u043d\u0456\u043d\u0430, 121",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_23": {
        "location": {
            "lat": 50.2660333,
            "lng": 28.7135585
        },
        "address": "\u0416\u0438\u0442\u043e\u043c\u0438\u0440, \u0432\u0443\u043b. \u041f\u0430\u0440\u0430\u0434\u0436\u0430\u043d\u043e\u0432\u0430, 55\u0430",
        "type": 1,
        "services": [{
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_24": {
        "location": {
            "lat": 50.499708,
            "lng": 30.168803
        },
        "address": "\u0417\u0430\u0431\u0443\u0447\u0447\u044f, \u0432\u0443\u043b. \u0422\u0435\u0445\u043d\u0456\u0447\u043d\u0430, 1",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_10",
            "name": "VILLA \u043c\u0430\u0440\u043a\u0435\u0442",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_25": {
        "location": {
            "lat": 50.5366354,
            "lng": 30.231847
        },
        "address": "\u0406\u0440\u043f\u0456\u043d\u044c, \u0432\u0443\u043b. \u0421\u043e\u0431\u043e\u0440\u043d\u0430, 195",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_10",
            "name": "VILLA \u043c\u0430\u0440\u043a\u0435\u0442",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_26": {
        "location": {
            "lat": 50.3764452,
            "lng": 30.7044697
        },
        "address": "\u041a\u0438\u0457\u0432, \u0410\u0432\u0442\u043e\u0442\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u043d\u0430, 1",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_27": {
        "location": {
            "lat": 50.3980905,
            "lng": 30.6287755
        },
        "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0411\u0430\u0436\u0430\u043d\u0430, 1\u0434",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_28": {
        "location": {
            "lat": 50.502868,
            "lng": 30.483575
        },
        "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0411\u043e\u0433\u0430\u0442\u0438\u0440\u0441\u044c\u043a\u0430, 1\u0433",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_29": {
        "location": {
            "lat": 50.5119618,
            "lng": 30.4845742
        },
        "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0411\u043e\u0433\u0430\u0442\u0438\u0440\u0441\u044c\u043a\u0430, 2",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_30": {
        "location": {
            "lat": 50.4934738,
            "lng": 30.4950358
        },
        "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0411\u043e\u0433\u0430\u0442\u0438\u0440\u0441\u044c\u043a\u0430, 2\u0434",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_12",
            "name": "KAWA DRIVE",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_31": {
        "location": {
            "lat": 50.458114,
            "lng": 30.617733
        },
        "address": "\u041a\u0438\u0457\u0432, \u0411\u0440\u043e\u0432\u0430\u0440\u0441\u044c\u043a\u0438\u0439 \u043f\u0440\u043e\u0441\u043f\u0435\u043a\u0442, 31-33",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_32": {
        "location": {
            "lat": 50.4520198,
            "lng": 30.4197942
        },
        "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0412\u0430\u0441\u0438\u043b\u0435\u043d\u043a\u0430, 3",
        "type": 1,
        "services": [{
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_33": {
        "location": {
            "lat": 50.4959745,
            "lng": 30.5837072
        },
        "address": "\u0410\u0417\u0421 \u0437\u0430\u0447\u0438\u043d\u0435\u043d\u0430 \u043d\u0430 \u0440\u0435\u043a\u043e\u043d\u0441\u0442\u0440\u0443\u043a\u0446\u0456\u044e",
        "type": 1,
        "services": []
    },
    "id-pole_34": {
        "location": {
            "lat": 50.3731986,
            "lng": 30.4621537
        },
        "address": "\u041a\u0438\u0457\u0432, \u0413\u043b\u0443\u0448\u043a\u043e\u0432\u0430 \u0430\u043a\u0430\u0434\u0435\u043c\u0456\u043a\u0430 \u043f\u0440\u043e\u0441\u043f\u0435\u043a\u0442, 10",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_35": {
        "location": {
            "lat": 50.4624384,
            "lng": 30.4999989
        },
        "address": " \u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0413\u043b\u0438\u0431\u043e\u0447\u0438\u0446\u044c\u043a\u0430, 53",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_36": {
        "location": {
            "lat": 50.4075944,
            "lng": 30.6047451
        },
        "address": "\u041a\u0438\u0457\u0432, \u0414\u043d\u0456\u043f\u0440\u043e\u0432\u0441\u044c\u043a\u0430 \u043d\u0430\u0431\u0435\u0440\u0435\u0436\u043d\u0430, 12\u0430",
        "type": 0,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }]
        }]
    },
    "id-pole_37": {
        "location": {
            "lat": 50.4105739,
            "lng": 30.6006829
        },
        "address": "\u041a\u0438\u0457\u0432, \u0414\u043d\u0456\u043f\u0440\u043e\u0432\u0441\u044c\u043a\u0430 \u043d\u0430\u0431\u0435\u0440\u0435\u0436\u043d\u0430, 17",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_38": {
        "location": {
            "lat": 50.3576947,
            "lng": 30.47512
        },
        "address": "\u041a\u0438\u0457\u0432, \u0417\u0430\u0431\u043e\u043b\u043e\u0442\u043d\u043e\u0433\u043e \u0430\u043a\u0430\u0434\u0435\u043c\u0456\u043a\u0430 \u043f\u0440\u043e\u0441\u043f\u0435\u043a\u0442, 15\u0434",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_39": {
        "location": {
            "lat": 50.4038293,
            "lng": 30.4897607
        },
        "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u041a\u0430\u0439\u0441\u0430\u0440\u043e\u0432\u0430, 15",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_40": {
        "location": {
            "lat": 50.451811,
            "lng": 30.6317774
        },
        "address": "\u041a\u0438\u0457\u0432, \u043f\u0440\u043e\u0432. \u041a\u0430\u0440\u0435\u043b\u044c\u0441\u044c\u043a\u0438\u0439, 3\u0430",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_41": {
        "location": {
            "lat": 50.4355829,
            "lng": 30.3592512
        },
        "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0412\u0435\u043b\u0438\u043a\u0430 \u041e\u043a\u0440\u0443\u0436\u043d\u0430, 2",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_42": {
        "location": {
            "lat": 50.4287491,
            "lng": 30.4675327
        },
        "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u041f\u0440\u0435\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0441\u044c\u043a\u0430, 1\u0430",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }]
        }]
    },
    "id-pole_43": {
        "location": {
            "lat": 50.4888632,
            "lng": 30.4941832
        },
        "address": "\u041a\u0438\u0457\u0432, \u0421\u0442\u0435\u043f\u0430\u043d\u0430 \u0411\u0430\u043d\u0434\u0435\u0440\u0438 \u043f\u0440\u043e\u0441\u043f\u0435\u043a\u0442, 6",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_44": {
        "location": {
            "lat": 50.4637047,
            "lng": 30.6004081
        },
        "address": "\u041a\u0438\u0457\u0432, \u0412\u0438\u0437\u0432\u043e\u043b\u0438\u0442\u0435\u043b\u0456\u0432 \u043f\u0440\u043e\u0441\u043f\u0435\u043a\u0442, 8\u0430",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_45": {
        "location": {
            "lat": 50.518188,
            "lng": 30.4684161
        },
        "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u041f\u043e\u043b\u044f\u0440\u043d\u0430, 12\/1",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_46": {
        "location": {
            "lat": 50.4969919,
            "lng": 30.4645408
        },
        "address": "\u0410\u0417\u0421 \u0437\u0430\u0447\u0438\u043d\u0435\u043d\u0430 \u043d\u0430 \u0440\u0435\u043a\u043e\u043d\u0441\u0442\u0440\u0443\u043a\u0446\u0456\u044e",
        "type": 1,
        "services": []
    },
    "id-pole_47": {
        "location": {
            "lat": 50.4048063,
            "lng": 30.4066063
        },
        "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0422\u0440\u0443\u0431\u043b\u0430\u0457\u043d\u0456 \u041c\u0438\u043a\u043e\u043b\u0438, 1\u0431",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_48": {
        "location": {
            "lat": 50.3089621,
            "lng": 29.0392189
        },
        "address": "\u041a\u043e\u0440\u043e\u0441\u0442\u0438\u0448\u0456\u0432, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430, 151",
        "type": 1,
        "services": [{
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_49": {
        "location": {
            "lat": 50.357729,
            "lng": 29.3207892
        },
        "address": "\u041a\u043e\u0447\u0435\u0440\u0456\u0432, \u0432\u0443\u043b. \u041b\u0456\u0441\u043e\u0432\u0430, 1\u0430",
        "type": 1,
        "services": [{
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_50": {
        "location": {
            "lat": 50.5957054,
            "lng": 30.0162356
        },
        "address": "\u041c\u0438\u043a\u0443\u043b\u0438\u0447\u0456, \u0432\u0443\u043b. \u041b\u0435\u043d\u0456\u043d\u0430, 49",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_51": {
        "location": {
            "lat": 50.0702073,
            "lng": 31.4640981
        },
        "address": "\u041f\u0435\u0440\u0435\u044f\u0441\u043b\u0430\u0432-\u0425\u043c\u0435\u043b\u044c\u043d\u0438\u0446\u044c\u043a\u0438\u0439 , \u0432\u0443\u043b. \u0427\u0435\u0440\u0432\u043e\u043d\u043e\u0430\u0440\u043c\u0456\u0439\u0446\u0456\u0432, 31",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_52": {
        "location": {
            "lat": 50.0675913,
            "lng": 31.4451671
        },
        "address": "\u041f\u0435\u0440\u0435\u044f\u0441\u043b\u0430\u0432-\u0425\u043c\u0435\u043b\u044c\u043d\u0438\u0446\u044c\u043a\u0438\u0439 , \u0432\u0443\u043b. \u0428\u043a\u0456\u043b\u044c\u043d\u0430, 51",
        "type": 1,
        "services": [{
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_53": {
        "location": {
            "lat": 49.6834053,
            "lng": 30.5008096
        },
        "address": "\u0420\u043e\u043a\u0438\u0442\u043d\u0435, \u0432\u0443\u043b. \u041b\u0435\u043d\u0456\u043d\u0430, 122",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_54": {
        "location": {
            "lat": 50.4469228,
            "lng": 30.2268863
        },
        "address": "\u0421\u0442\u043e\u044f\u043d\u043a\u0430, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432-\u0427\u043e\u043f, 20-\u0439 \u043a\u043c",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_55": {
        "location": {
            "lat": 50.1243636,
            "lng": 30.8258772
        },
        "address": "\u0425\u0430\u043b\u0435\u043f'\u044f, \u0432\u0443\u043b. \u0428\u0435\u0432\u0447\u0435\u043d\u043a\u0430, 210",
        "type": 0,
        "services": []
    },
    "id-pole_56": {
        "location": {
            "lat": 50.3337046,
            "lng": 30.4056896
        },
        "address": "\u0427\u0430\u0431\u0430\u043d\u0438, \u0432\u0443\u043b. \u041f\u043e\u043a\u0440\u043e\u0432\u0441\u044c\u043a\u0430, 162\u0430",
        "type": 1,
        "services": [{
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_57": {
        "location": {
            "lat": 50.3345892,
            "lng": 30.4046517
        },
        "address": "\u0427\u0430\u0431\u0430\u043d\u0438, \u041a\u0438\u0457\u0432-\u041e\u0434\u0435\u0441\u0430 17\u043a\u043c+170\u043c",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_58": {
        "location": {
            "lat": 50.2829673,
            "lng": 31.7616805
        },
        "address": "\u042f\u0433\u043e\u0442\u0438\u043d, \u0432\u0443\u043b. \u041d\u0435\u0437\u0430\u043b\u0435\u0436\u043d\u043e\u0441\u0442\u0456, 162",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_59": {
        "location": {
            "lat": 50.0586214,
            "lng": 31.5011595
        },
        "address": "\u041a\u0438\u0457\u0432-\u0411\u043e\u0440\u0438\u0441\u043f\u0456\u043b\u044c-\u0414\u043d\u0456\u043f\u0440\u043e\u043f\u0435\u0442\u0440\u043e\u0432\u0441\u044c\u043a-\u0417\u0430\u043f\u043e\u0440\u0456\u0436\u0436\u044f, 54-\u0439 \u043a\u043c",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_60": {
        "location": {
            "lat": 49.8712317,
            "lng": 31.731279
        },
        "address": "\u041a\u0438\u0457\u0432-\u0411\u043e\u0440\u0438\u0441\u043f\u0456\u043b\u044c-\u0414\u043d\u0456\u043f\u0440\u043e\u043f\u0435\u0442\u0440\u043e\u0432\u0441\u044c\u043a-\u0417\u0430\u043f\u043e\u0440\u0456\u0436\u0436\u044f, 80-\u0439 \u043a\u043c",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_61": {
        "location": {
            "lat": 50.2059483,
            "lng": 31.7761436
        },
        "address": "\u041f\u0430\u043d\u0444\u0438\u043b\u0438, \u041a\u0438\u0435\u0432\u0441\u043a\u0430\u044f \u0443\u043b\u0438\u0446\u0430, 2 (102-\u0439 \u043a\u043c \u0448\u043e\u0441\u0435 \u041a\u0438\u0457\u0432-\u0425\u0430\u0440\u043a\u0456\u0432)",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_62": {
        "location": {
            "lat": 50.4109106,
            "lng": 30.3835809
        },
        "address": "\u0421\u043e\u0444\u0438\u0432\u0441\u043a\u0430\u044f \u0411\u043e\u0440\u0449\u0430\u0433\u043e\u0432\u043a\u0430, \u0432\u0443\u043b. \u041a\u0438\u0435\u0432\u0441\u043a\u0430\u044f, 2",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_63": {
        "location": {
            "lat": 50.4956883,
            "lng": 30.4055965
        },
        "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0413\u0440\u0435\u0447\u043a\u0430, 13\u0430",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_64": {
        "location": {
            "lat": 50.2213301,
            "lng": 31.6315224
        },
        "address": "91\u043a\u043c \u0448\u043e\u0441\u0441\u0435 \u041a\u0438\u0435\u0432-\u0425\u0430\u0440\u044c\u043a\u043e\u0432, \u0441. \u0416\u043e\u0432\u0442\u043d\u0435\u0432\u043e\u0435",
        "type": 1,
        "services": [{
            "idService": "id_13",
            "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "95 E40",
                "color": "#74c600"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_65": {
        "location": {
            "lat": 50.2284208,
            "lng": 32.1104843
        },
        "address": "126 \u043a\u043c. \u041a\u0438\u0457\u0432 - \u0425\u0430\u0440\u043a\u0456\u0432",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_66": {
        "location": {
            "lat": 49.8704086,
            "lng": 31.7317274
        },
        "address": "\u041a\u0438\u0457\u0432-\u0411\u043e\u0440\u0438\u0441\u043f\u0456\u043b\u044c-\u0414\u043d\u0456\u043f\u0440\u043e\u043f\u0435\u0442\u0440\u043e\u0432\u0441\u044c\u043a-\u0417\u0430\u043f\u043e\u0440\u0456\u0436\u0436\u044f, 80-\u0439 \u043a\u043c",
        "type": 1,
        "services": [{
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_67": {
        "location": {
            "lat": 50.489145,
            "lng": 30.505109
        },
        "address": "\u041a\u0438\u0457\u0432, \u043f\u0440. \u0421\u0442\u0435\u043f\u0430\u043d\u0430 \u0411\u0430\u043d\u0434\u0435\u0440\u0438 29\u0410",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    },
    "id-pole_68": {
        "location": {
            "lat": 50.385353,
            "lng": 30.351402
        },
        "address": "\u0412\u0438\u0448\u043d\u0435\u0432\u0435 \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430, 11\u0430",
        "type": 1,
        "services": [{
            "idService": "id_11",
            "name": "KAWABAR",
            "type": "horeca"
        }, {
            "idService": "id_9",
            "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
            "type": "shop"
        }, {
            "idService": "id_17",
            "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
            "type": "fuel",
            "fuelKinds": [{
                "name": "95 UA",
                "color": "#63bce8"
            }, {
                "name": "SUPER 95",
                "color": "#b35dcc"
            }, {
                "name": "VENTUS 95",
                "color": "#fe5755"
            }, {
                "name": "VENTUS Diesel",
                "color": "#969492"
            }, {
                "name": "92",
                "color": "#b3b3b3"
            }, {
                "name": "\u0414\u041f",
                "color": "#b3b3b3"
            }, {
                "name": "\u0413\u0410\u0417",
                "color": "#b3b3b3"
            }]
        }]
    }
};
var fieldsJSON = {
    "ChIJ94pF_F3O1EARB10ge68K4KY": {
        "ChIJO3Hqs9xV1EARTgZPq0DoT9k": {
            "id-pole_6": {
                "location": {
                    "lat": 50.3245637,
                    "lng": 31.5122443
                },
                "address": "\u0411\u0435\u0440\u0435\u0437\u0430\u043d\u044c, \u0443\u043b. \u041c\u0430\u044f\u043a\u043e\u0432\u0441\u044c\u043a\u043e\u0433\u043e, 10",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJeSXPd67o1EAR2BnilQK1gfo": {
            "id-pole_9": {
                "location": {
                    "lat": 50.3658265,
                    "lng": 30.9210091
                },
                "address": "\u0411\u043e\u0440\u0438\u0441\u043f\u0456\u043b\u044c, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u043a\u0438\u0439 \u0448\u043b\u044f\u0445, 2\u0431",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_10": {
                "location": {
                    "lat": 50.3657993,
                    "lng": 30.9855571
                },
                "address": "\u0411\u043e\u0440\u0438\u0441\u043f\u0456\u043b\u044c, \u0432\u0443\u043b. \u041f\u0440\u0438\u0432\u043e\u043a\u0437\u0430\u043b\u044c\u043d\u0430, 88",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJ7xzVfdHb1EARiZaO_Nyy6_Y": {
            "id-pole_11": {
                "location": {
                    "lat": 50.4913414,
                    "lng": 30.741289
                },
                "address": "\u0411\u0440\u043e\u0432\u0430\u0440\u0438, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430, 2\u0430",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_12": {
                "location": {
                    "lat": 50.5143181,
                    "lng": 30.7827679
                },
                "address": "\u0411\u0440\u043e\u0432\u0430\u0440\u0438, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430, 227",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_13": {
                "location": {
                    "lat": 50.5271161,
                    "lng": 30.7936507
                },
                "address": "\u0411\u0440\u043e\u0432\u0430\u0440\u0438, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430, 320",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_14": {
                "location": {
                    "lat": 50.5049746,
                    "lng": 30.7945041
                },
                "address": "\u0411\u0440\u043e\u0432\u0430\u0440\u0438, \u0432\u0443\u043b. \u042f\u0440\u043e\u0441\u043b\u0430\u0432\u0430 \u041c\u0443\u0434\u0440\u043e\u0433\u043e, 57",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_15": {
                "location": {
                    "lat": 50.5174787,
                    "lng": 30.8087298
                },
                "address": "\u0411\u0440\u043e\u0432\u0430\u0440\u0438, \u0421\u0438\u043c\u043e\u043d\u0430 \u041f\u0435\u0442\u043b\u044e\u0440\u0438, 11\u0431",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_16": {
                "location": {
                    "lat": 50.5107235,
                    "lng": 30.8455619
                },
                "address": "\u0411\u0440\u043e\u0432\u0430\u0440\u0438, \u0432\u0443\u043b. \u0411\u0440\u043e\u0432\u0430\u0440\u0441\u044c\u043a\u043e\u0457 \u0441\u043e\u0442\u043d\u0456, 15\u0430",
                "type": 1,
                "services": [{
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJdxka4Uql1EARLwfu5DopyVQ": {
            "id-pole_17": {
                "location": {
                    "lat": 50.2328435,
                    "lng": 30.3216956
                },
                "address": "\u0412\u0430\u0441\u0438\u043b\u044c\u043a\u0456\u0432, \u0432\u0443\u043b. \u0414\u0435\u043a\u0430\u0431\u0440\u0438\u0441\u0442\u0456\u0432, 406",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJM8wmjybK1EAR5CLzKWJ9lF8": {
            "id-pole_18": {
                "location": {
                    "lat": 50.3917253,
                    "lng": 30.3630996
                },
                "address": "\u0412\u0438\u0448\u043d\u0435\u0432\u0435, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430, 2\u0433",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_19": {
                "location": {
                    "lat": 50.3882889,
                    "lng": 30.3921962
                },
                "address": "\u0412\u0438\u0448\u043d\u0435\u0432\u0435, \u0432\u0443\u043b. \u0427\u043e\u0440\u043d\u043e\u0432\u043e\u043b\u0430, 54",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_14",
                    "name": "\u0411\u043e\u0440\u0449BURGER",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_68": {
                "location": {
                    "lat": 50.385353,
                    "lng": 30.351402
                },
                "address": "\u0412\u0438\u0448\u043d\u0435\u0432\u0435 \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430, 11\u0430",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJgz3-him31EAR_ayvafT3_w8": {
            "id-pole_20": {
                "location": {
                    "lat": 50.2546003,
                    "lng": 30.3047588
                },
                "address": "\u0413\u043b\u0435\u0432\u0430\u0445\u0430, \u0432\u0443\u043b. \u0412\u043e\u043a\u0437\u0430\u043b\u044c\u043d\u0430, 2\u0430",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJAxe6Puly00AR8sVxE6JhWxA": {
            "id-pole_21": {
                "location": {
                    "lat": 49.6553303,
                    "lng": 30.3478016
                },
                "address": "\u0414\u043e\u0432\u0433\u0430\u043b\u0456\u0432\u0441\u044c\u043a\u0435, \u0432\u0443\u043b. \u0414\u0432\u043e\u0440\u044f\u043d\u0441\u044c\u043a\u0430, 1\u0430",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJX43JuiIn1UARhsfNfHwpjuc": {
            "id-pole_22": {
                "location": {
                    "lat": 50.586397,
                    "lng": 30.83959
                },
                "address": "\u0412\u0435\u043b\u0438\u043a\u0430 \u0414\u0438\u043c\u0435\u0440\u043a\u0430, \u0432\u0443\u043b. \u041b\u0435\u043d\u0456\u043d\u0430, 121",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJr9_TxLM2K0cRSPSB7gOclMA": {
            "id-pole_24": {
                "location": {
                    "lat": 50.499708,
                    "lng": 30.168803
                },
                "address": "\u0417\u0430\u0431\u0443\u0447\u0447\u044f, \u0432\u0443\u043b. \u0422\u0435\u0445\u043d\u0456\u0447\u043d\u0430, 1",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_10",
                    "name": "VILLA \u043c\u0430\u0440\u043a\u0435\u0442",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJ29VN6RYyK0cRNI9p50Rp41w": {
            "id-pole_25": {
                "location": {
                    "lat": 50.5366354,
                    "lng": 30.231847
                },
                "address": "\u0406\u0440\u043f\u0456\u043d\u044c, \u0432\u0443\u043b. \u0421\u043e\u0431\u043e\u0440\u043d\u0430, 195",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_10",
                    "name": "VILLA \u043c\u0430\u0440\u043a\u0435\u0442",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJBUVa4U7P1EAR_kYBF9IxSXY": {
            "id-pole_26": {
                "location": {
                    "lat": 50.3764452,
                    "lng": 30.7044697
                },
                "address": "\u041a\u0438\u0457\u0432, \u0410\u0432\u0442\u043e\u0442\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u043d\u0430, 1",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_27": {
                "location": {
                    "lat": 50.3980905,
                    "lng": 30.6287755
                },
                "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0411\u0430\u0436\u0430\u043d\u0430, 1\u0434",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_28": {
                "location": {
                    "lat": 50.502868,
                    "lng": 30.483575
                },
                "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0411\u043e\u0433\u0430\u0442\u0438\u0440\u0441\u044c\u043a\u0430, 1\u0433",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_29": {
                "location": {
                    "lat": 50.5119618,
                    "lng": 30.4845742
                },
                "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0411\u043e\u0433\u0430\u0442\u0438\u0440\u0441\u044c\u043a\u0430, 2",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_30": {
                "location": {
                    "lat": 50.4934738,
                    "lng": 30.4950358
                },
                "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0411\u043e\u0433\u0430\u0442\u0438\u0440\u0441\u044c\u043a\u0430, 2\u0434",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_12",
                    "name": "KAWA DRIVE",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_31": {
                "location": {
                    "lat": 50.458114,
                    "lng": 30.617733
                },
                "address": "\u041a\u0438\u0457\u0432, \u0411\u0440\u043e\u0432\u0430\u0440\u0441\u044c\u043a\u0438\u0439 \u043f\u0440\u043e\u0441\u043f\u0435\u043a\u0442, 31-33",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_32": {
                "location": {
                    "lat": 50.4520198,
                    "lng": 30.4197942
                },
                "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0412\u0430\u0441\u0438\u043b\u0435\u043d\u043a\u0430, 3",
                "type": 1,
                "services": [{
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_33": {
                "location": {
                    "lat": 50.4959745,
                    "lng": 30.5837072
                },
                "address": "\u0410\u0417\u0421 \u0437\u0430\u0447\u0438\u043d\u0435\u043d\u0430 \u043d\u0430 \u0440\u0435\u043a\u043e\u043d\u0441\u0442\u0440\u0443\u043a\u0446\u0456\u044e",
                "type": 1,
                "services": []
            },
            "id-pole_34": {
                "location": {
                    "lat": 50.3731986,
                    "lng": 30.4621537
                },
                "address": "\u041a\u0438\u0457\u0432, \u0413\u043b\u0443\u0448\u043a\u043e\u0432\u0430 \u0430\u043a\u0430\u0434\u0435\u043c\u0456\u043a\u0430 \u043f\u0440\u043e\u0441\u043f\u0435\u043a\u0442, 10",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_35": {
                "location": {
                    "lat": 50.4624384,
                    "lng": 30.4999989
                },
                "address": " \u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0413\u043b\u0438\u0431\u043e\u0447\u0438\u0446\u044c\u043a\u0430, 53",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_36": {
                "location": {
                    "lat": 50.4075944,
                    "lng": 30.6047451
                },
                "address": "\u041a\u0438\u0457\u0432, \u0414\u043d\u0456\u043f\u0440\u043e\u0432\u0441\u044c\u043a\u0430 \u043d\u0430\u0431\u0435\u0440\u0435\u0436\u043d\u0430, 12\u0430",
                "type": 0,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }]
                }]
            },
            "id-pole_37": {
                "location": {
                    "lat": 50.4105739,
                    "lng": 30.6006829
                },
                "address": "\u041a\u0438\u0457\u0432, \u0414\u043d\u0456\u043f\u0440\u043e\u0432\u0441\u044c\u043a\u0430 \u043d\u0430\u0431\u0435\u0440\u0435\u0436\u043d\u0430, 17",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_38": {
                "location": {
                    "lat": 50.3576947,
                    "lng": 30.47512
                },
                "address": "\u041a\u0438\u0457\u0432, \u0417\u0430\u0431\u043e\u043b\u043e\u0442\u043d\u043e\u0433\u043e \u0430\u043a\u0430\u0434\u0435\u043c\u0456\u043a\u0430 \u043f\u0440\u043e\u0441\u043f\u0435\u043a\u0442, 15\u0434",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_39": {
                "location": {
                    "lat": 50.4038293,
                    "lng": 30.4897607
                },
                "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u041a\u0430\u0439\u0441\u0430\u0440\u043e\u0432\u0430, 15",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_40": {
                "location": {
                    "lat": 50.451811,
                    "lng": 30.6317774
                },
                "address": "\u041a\u0438\u0457\u0432, \u043f\u0440\u043e\u0432. \u041a\u0430\u0440\u0435\u043b\u044c\u0441\u044c\u043a\u0438\u0439, 3\u0430",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_41": {
                "location": {
                    "lat": 50.4355829,
                    "lng": 30.3592512
                },
                "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0412\u0435\u043b\u0438\u043a\u0430 \u041e\u043a\u0440\u0443\u0436\u043d\u0430, 2",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_42": {
                "location": {
                    "lat": 50.4287491,
                    "lng": 30.4675327
                },
                "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u041f\u0440\u0435\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0441\u044c\u043a\u0430, 1\u0430",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }]
                }]
            },
            "id-pole_43": {
                "location": {
                    "lat": 50.4888632,
                    "lng": 30.4941832
                },
                "address": "\u041a\u0438\u0457\u0432, \u0421\u0442\u0435\u043f\u0430\u043d\u0430 \u0411\u0430\u043d\u0434\u0435\u0440\u0438 \u043f\u0440\u043e\u0441\u043f\u0435\u043a\u0442, 6",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_44": {
                "location": {
                    "lat": 50.4637047,
                    "lng": 30.6004081
                },
                "address": "\u041a\u0438\u0457\u0432, \u0412\u0438\u0437\u0432\u043e\u043b\u0438\u0442\u0435\u043b\u0456\u0432 \u043f\u0440\u043e\u0441\u043f\u0435\u043a\u0442, 8\u0430",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_45": {
                "location": {
                    "lat": 50.518188,
                    "lng": 30.4684161
                },
                "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u041f\u043e\u043b\u044f\u0440\u043d\u0430, 12\/1",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_46": {
                "location": {
                    "lat": 50.4969919,
                    "lng": 30.4645408
                },
                "address": "\u0410\u0417\u0421 \u0437\u0430\u0447\u0438\u043d\u0435\u043d\u0430 \u043d\u0430 \u0440\u0435\u043a\u043e\u043d\u0441\u0442\u0440\u0443\u043a\u0446\u0456\u044e",
                "type": 1,
                "services": []
            },
            "id-pole_47": {
                "location": {
                    "lat": 50.4048063,
                    "lng": 30.4066063
                },
                "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0422\u0440\u0443\u0431\u043b\u0430\u0457\u043d\u0456 \u041c\u0438\u043a\u043e\u043b\u0438, 1\u0431",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_63": {
                "location": {
                    "lat": 50.4956883,
                    "lng": 30.4055965
                },
                "address": "\u041a\u0438\u0457\u0432, \u0432\u0443\u043b. \u0413\u0440\u0435\u0447\u043a\u0430, 13\u0430",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_67": {
                "location": {
                    "lat": 50.489145,
                    "lng": 30.505109
                },
                "address": "\u041a\u0438\u0457\u0432, \u043f\u0440. \u0421\u0442\u0435\u043f\u0430\u043d\u0430 \u0411\u0430\u043d\u0434\u0435\u0440\u0438 29\u0410",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJpV5SJPk6K0cRHIK_QzciQSI": {
            "id-pole_50": {
                "location": {
                    "lat": 50.5957054,
                    "lng": 30.0162356
                },
                "address": "\u041c\u0438\u043a\u0443\u043b\u0438\u0447\u0456, \u0432\u0443\u043b. \u041b\u0435\u043d\u0456\u043d\u0430, 49",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJtRyQHN5C1EARkMzkPfFQJAk": {
            "id-pole_51": {
                "location": {
                    "lat": 50.0702073,
                    "lng": 31.4640981
                },
                "address": "\u041f\u0435\u0440\u0435\u044f\u0441\u043b\u0430\u0432-\u0425\u043c\u0435\u043b\u044c\u043d\u0438\u0446\u044c\u043a\u0438\u0439 , \u0432\u0443\u043b. \u0427\u0435\u0440\u0432\u043e\u043d\u043e\u0430\u0440\u043c\u0456\u0439\u0446\u0456\u0432, 31",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_52": {
                "location": {
                    "lat": 50.0675913,
                    "lng": 31.4451671
                },
                "address": "\u041f\u0435\u0440\u0435\u044f\u0441\u043b\u0430\u0432-\u0425\u043c\u0435\u043b\u044c\u043d\u0438\u0446\u044c\u043a\u0438\u0439 , \u0432\u0443\u043b. \u0428\u043a\u0456\u043b\u044c\u043d\u0430, 51",
                "type": 1,
                "services": [{
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_59": {
                "location": {
                    "lat": 50.0586214,
                    "lng": 31.5011595
                },
                "address": "\u041a\u0438\u0457\u0432-\u0411\u043e\u0440\u0438\u0441\u043f\u0456\u043b\u044c-\u0414\u043d\u0456\u043f\u0440\u043e\u043f\u0435\u0442\u0440\u043e\u0432\u0441\u044c\u043a-\u0417\u0430\u043f\u043e\u0440\u0456\u0436\u0436\u044f, 54-\u0439 \u043a\u043c",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJ5XQ4zVNx00ARNlbrNxZ-DZ4": {
            "id-pole_53": {
                "location": {
                    "lat": 49.6834053,
                    "lng": 30.5008096
                },
                "address": "\u0420\u043e\u043a\u0438\u0442\u043d\u0435, \u0432\u0443\u043b. \u041b\u0435\u043d\u0456\u043d\u0430, 122",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJY2Oad2A0K0cRluBqaWn9dt4": {
            "id-pole_54": {
                "location": {
                    "lat": 50.4469228,
                    "lng": 30.2268863
                },
                "address": "\u0421\u0442\u043e\u044f\u043d\u043a\u0430, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432-\u0427\u043e\u043f, 20-\u0439 \u043a\u043c",
                "type": 1,
                "services": [{
                    "idService": "id_11",
                    "name": "KAWABAR",
                    "type": "horeca"
                }, {
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJcT98WN6R1EARtlm8MHRUy6U": {
            "id-pole_55": {
                "location": {
                    "lat": 50.1243636,
                    "lng": 30.8258772
                },
                "address": "\u0425\u0430\u043b\u0435\u043f'\u044f, \u0432\u0443\u043b. \u0428\u0435\u0432\u0447\u0435\u043d\u043a\u0430, 210",
                "type": 0,
                "services": []
            }
        },
        "ChIJuQeULC_I1EARDoSPCra6XpE": {
            "id-pole_56": {
                "location": {
                    "lat": 50.3337046,
                    "lng": 30.4056896
                },
                "address": "\u0427\u0430\u0431\u0430\u043d\u0438, \u0432\u0443\u043b. \u041f\u043e\u043a\u0440\u043e\u0432\u0441\u044c\u043a\u0430, 162\u0430",
                "type": 1,
                "services": [{
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_57": {
                "location": {
                    "lat": 50.3345892,
                    "lng": 30.4046517
                },
                "address": "\u0427\u0430\u0431\u0430\u043d\u0438, \u041a\u0438\u0457\u0432-\u041e\u0434\u0435\u0441\u0430 17\u043a\u043c+170\u043c",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJd5Y_-x9N1EAR9xj4J6R7_pI": {
            "id-pole_58": {
                "location": {
                    "lat": 50.2829673,
                    "lng": 31.7616805
                },
                "address": "\u042f\u0433\u043e\u0442\u0438\u043d, \u0432\u0443\u043b. \u041d\u0435\u0437\u0430\u043b\u0435\u0436\u043d\u043e\u0441\u0442\u0456, 162",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJXYnNs9AW1EARWhFJJLEhMu0": {
            "id-pole_60": {
                "location": {
                    "lat": 49.8712317,
                    "lng": 31.731279
                },
                "address": "\u041a\u0438\u0457\u0432-\u0411\u043e\u0440\u0438\u0441\u043f\u0456\u043b\u044c-\u0414\u043d\u0456\u043f\u0440\u043e\u043f\u0435\u0442\u0440\u043e\u0432\u0441\u044c\u043a-\u0417\u0430\u043f\u043e\u0440\u0456\u0436\u0436\u044f, 80-\u0439 \u043a\u043c",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_66": {
                "location": {
                    "lat": 49.8704086,
                    "lng": 31.7317274
                },
                "address": "\u041a\u0438\u0457\u0432-\u0411\u043e\u0440\u0438\u0441\u043f\u0456\u043b\u044c-\u0414\u043d\u0456\u043f\u0440\u043e\u043f\u0435\u0442\u0440\u043e\u0432\u0441\u044c\u043a-\u0417\u0430\u043f\u043e\u0440\u0456\u0436\u0436\u044f, 80-\u0439 \u043a\u043c",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJuwkGLfxL1EAR8MRJJBcRq5o": {
            "id-pole_61": {
                "location": {
                    "lat": 50.2059483,
                    "lng": 31.7761436
                },
                "address": "\u041f\u0430\u043d\u0444\u0438\u043b\u0438, \u041a\u0438\u0435\u0432\u0441\u043a\u0430\u044f \u0443\u043b\u0438\u0446\u0430, 2 (102-\u0439 \u043a\u043c \u0448\u043e\u0441\u0435 \u041a\u0438\u0457\u0432-\u0425\u0430\u0440\u043a\u0456\u0432)",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            },
            "id-pole_65": {
                "location": {
                    "lat": 50.2284208,
                    "lng": 32.1104843
                },
                "address": "126 \u043a\u043c. \u041a\u0438\u0457\u0432 - \u0425\u0430\u0440\u043a\u0456\u0432",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJ6ZuohbvL1EARBi69rgUtfCE": {
            "id-pole_62": {
                "location": {
                    "lat": 50.4109106,
                    "lng": 30.3835809
                },
                "address": "\u0421\u043e\u0444\u0438\u0432\u0441\u043a\u0430\u044f \u0411\u043e\u0440\u0449\u0430\u0433\u043e\u0432\u043a\u0430, \u0432\u0443\u043b. \u041a\u0438\u0435\u0432\u0441\u043a\u0430\u044f, 2",
                "type": 1,
                "services": [{
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJh8ROnOSwLEcR_Thg6616sew": {
            "id-pole_64": {
                "location": {
                    "lat": 50.2213301,
                    "lng": 31.6315224
                },
                "address": "91\u043a\u043c \u0448\u043e\u0441\u0441\u0435 \u041a\u0438\u0435\u0432-\u0425\u0430\u0440\u044c\u043a\u043e\u0432, \u0441. \u0416\u043e\u0432\u0442\u043d\u0435\u0432\u043e\u0435",
                "type": 1,
                "services": [{
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        }
    },
    "ChIJT7SGL2DAKkERMEH2iIQGAQE": {
        "ChIJ6e5UiwaiKkERgBydX8APFHg": {
            "id-pole_8": {
                "location": {
                    "lat": 51.1383785,
                    "lng": 31.7776322
                },
                "address": "\u0411\u043e\u0431\u0440\u0438\u043a, \u0432\u0443\u043b. \u041d\u0430\u0431\u0435\u0440\u0435\u0436\u043d\u0430, 21",
                "type": 1,
                "services": [{
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        }
    },
    "ChIJC9KIyxTxK0cRoED2iIQGAQE": {
        "ChIJXTX6K6NkLEcRKeK52aPSSvE": {
            "id-pole_23": {
                "location": {
                    "lat": 50.2660333,
                    "lng": 28.7135585
                },
                "address": "\u0416\u0438\u0442\u043e\u043c\u0438\u0440, \u0432\u0443\u043b. \u041f\u0430\u0440\u0430\u0434\u0436\u0430\u043d\u043e\u0432\u0430, 55\u0430",
                "type": 1,
                "services": [{
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJeTaysimCLEcRcbTGl-C8QdA": {
            "id-pole_48": {
                "location": {
                    "lat": 50.3089621,
                    "lng": 29.0392189
                },
                "address": "\u041a\u043e\u0440\u043e\u0441\u0442\u0438\u0448\u0456\u0432, \u0432\u0443\u043b. \u041a\u0438\u0457\u0432\u0441\u044c\u043a\u0430, 151",
                "type": 1,
                "services": [{
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        },
        "ChIJF-HuCppjK0cRpbWAhHDdZdc": {
            "id-pole_49": {
                "location": {
                    "lat": 50.357729,
                    "lng": 29.3207892
                },
                "address": "\u041a\u043e\u0447\u0435\u0440\u0456\u0432, \u0432\u0443\u043b. \u041b\u0456\u0441\u043e\u0432\u0430, 1\u0430",
                "type": 1,
                "services": [{
                    "idService": "id_13",
                    "name": "\u00ab\u0411\u043e\u0440\u0449-\u043a\u0430\u0444\u0435\u00bb",
                    "type": "horeca"
                }, {
                    "idService": "id_9",
                    "name": "\u00ab\u041f\u043e\u0434\u043e\u0440\u043e\u0436\u043d\u0438\u043a\u00bb",
                    "type": "shop"
                }, {
                    "idService": "id_17",
                    "name": "\u0422\u043e\u043f\u043b\u0438\u0432\u043e",
                    "type": "fuel",
                    "fuelKinds": [{
                        "name": "95 UA",
                        "color": "#63bce8"
                    }, {
                        "name": "SUPER 95",
                        "color": "#b35dcc"
                    }, {
                        "name": "VENTUS 95",
                        "color": "#fe5755"
                    }, {
                        "name": "95 E40",
                        "color": "#74c600"
                    }, {
                        "name": "VENTUS Diesel",
                        "color": "#969492"
                    }, {
                        "name": "92",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0414\u041f",
                        "color": "#b3b3b3"
                    }, {
                        "name": "\u0413\u0410\u0417",
                        "color": "#b3b3b3"
                    }]
                }]
            }
        }
    }
};

var rectKyiv = {
	latMin: 50.508186,
	lngMin: 30.407528,
	latMax: 50.4044414,
	lngMax: 30.6846903
};
